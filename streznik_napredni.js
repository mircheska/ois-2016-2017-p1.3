// Zaradi združljivosti razvoja na lokalnem računalniku oz. Cloud9 okolju
var port = process.env.PORT || 8080;

// Opredelimo knjižnice, ki jih potrebujemo 
var mime = require('mime');
var formidable = require('formidable');
var http = require('http');
var fs = require('fs');
var path = require('path');

var predpomnilnik = {};

// Kreiramo objekt strežnika in mu nastavimo osnove parametre
var streznik = http.createServer(function(zahteva, odgovor) {
  var potDoDatoteke = false;

  if (zahteva.url == '/') {
    potDoDatoteke = './public/index.html';
  } else if (zahteva.url == '/posredujSporocilo') {
    obdelajSporocilo(zahteva, odgovor);
  } else {
    potDoDatoteke = './public' + zahteva.url;
  }
  
  // Statično vsebino postrežemo le takrat, ko gre za zahtevo za takšno stran
  if (potDoDatoteke)
    posredujStaticnoVsebino(odgovor, predpomnilnik, potDoDatoteke);
});

// Na tej točki strežnik poženemo
streznik.listen(port, function() {
  console.log('Strežnik je pognan.');
});

// Metoda za posredovanje statične vsebine (datotek iz mape public)
function posredujStaticnoVsebino(odgovor, predpomnilnik, absolutnaPotDoDatoteke) {
  // Preverjanje ali je datoteka v predpomnilniku
  if (predpomnilnik[absolutnaPotDoDatoteke]) {
    posredujDatoteko(odgovor, absolutnaPotDoDatoteke, predpomnilnik[absolutnaPotDoDatoteke]);
  } else {
    fs.exists(absolutnaPotDoDatoteke, function(datotekaObstaja) {
      if (datotekaObstaja) {
        fs.readFile(absolutnaPotDoDatoteke, function(napaka, datotekaVsebina) {
          if (napaka) {
            posredujNapako500(odgovor);
          } else {
            // Shranjevanje vsebine nove datoteke v predpomnilnik
            predpomnilnik[absolutnaPotDoDatoteke] = datotekaVsebina;
            posredujDatoteko(odgovor, absolutnaPotDoDatoteke, datotekaVsebina);
          }
        });
      } else {
        posredujNapako404(odgovor);
      }
    });
  }
}

// Metoda, ki se kliče, ko zahtevamo datoteko, ki ne obstaja
function posredujNapako404(odgovor) {
  odgovor.writeHead(404, {'Content-Type': 'text/plain'});
  odgovor.write('Napaka 404: Vira ni mogoče najti.');
  odgovor.end();
}

// Metoda, ki se kliče, ko pride na strežniku do napake
function posredujNapako500(odgovor) {
  odgovor.writeHead(500, {'Content-Type': 'text/plain'});
  odgovor.write('Napaka 500: Prišlo je do napake strežnika.');
  odgovor.end();
}

// Metoda, ki vrne datoteko in nastavi tip datoteke, da jo zna brskalnik ustrezno prikazati
function posredujDatoteko(odgovor, datotekaPot, datotekaVsebina) {
  odgovor.writeHead(200, {'Content-Type': mime.lookup(path.basename(datotekaPot))});
  odgovor.end(datotekaVsebina);
}

// Metoda, ki obdela posredovana sporočila na strežnik
function obdelajSporocilo(zahteva, odgovor) {
  var obrazec = new formidable.IncomingForm();
  obrazec.parse(zahteva, function (napaka, vrednosti) {
    if (napaka) {
      posredujNapako404(odgovor);
    } else {
      var sporociloUporabnika = new Date().toJSON() + '|' + vrednosti.oseba + '|' + 
        vrednosti.komentar + '|' + vrednosti.strah + '\n';
      fs.appendFile('./sporocila.txt', sporociloUporabnika, function (napaka) {
        if (napaka) {
          posredujNapako500(odgovor);
        } else {
          posredujZahvaloZaPosredovaneKomentarje(odgovor);
        }
      });
    }
  });
}

// Metoda, ki se zahvali uporabniku
function posredujZahvaloZaPosredovaneKomentarje(odgovor) {
  odgovor.writeHead(200, {'Content-Type': 'text/plain'});
  odgovor.write('Hvala za posredovane komentarje!');
  odgovor.end();
}